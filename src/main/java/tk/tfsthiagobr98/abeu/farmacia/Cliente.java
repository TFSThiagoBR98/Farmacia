package tk.tfsthiagobr98.abeu.farmacia;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.time.LocalDate;

public class Cliente {
    private String cpf;
    private String nome;
    private LocalDate dt_Nascimento;
    private String email;
    private long telefone;

    // Banco de dados

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
            "jdbc:mysql://127.0.0.1:3306/farmacia", "root", "tfs1112uyt"
        );
    }

    public void salvar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "insert into cliente "
                    + "(cpf, nome, dt_nascimento, email, telefone)"
                    + "values (?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, cpf);
            ps.setString(2, nome);
            ps.setDate(3, Date.valueOf(dt_Nascimento));
            ps.setString(4, email);
            ps.setLong(5, telefone);
            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante o salvamento de cliente");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void alterar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "update cliente "
                    + "set nome=?, dt_nascimento=?, email=?, telefone=? "
                    + "where (cpf = ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, nome);
            ps.setDate(2, Date.valueOf(dt_Nascimento));
            ps.setString(3, email);
            ps.setLong(4, telefone);
            ps.setString(5, cpf);
            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a alteração de cliente");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void excluir() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "delete from cliente where (cpf = ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, cpf);
            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a exclusão de cliente");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    // Construtores

    public Cliente() {}

    public Cliente(String CPF, String nome, LocalDate dt_nascimento, String email, long telefone) {
        this.setCPF(CPF);
        this.nome = nome;
        this.dt_Nascimento = dt_nascimento;
        this.email = email;
        this.telefone = telefone;
    }

    // Encapsulamentos

    public String getCPF() {
        return cpf;
    }

    public void setCPF(String CPF) {
        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") || CPF.equals("11111111111") ||
                CPF.equals("22222222222") || CPF.equals("33333333333") ||
                CPF.equals("44444444444") || CPF.equals("55555555555") ||
                CPF.equals("66666666666") || CPF.equals("77777777777") ||
                CPF.equals("88888888888") || CPF.equals("99999999999") ||
                (CPF.length() != 11))
            throw new InvalidParameterException();

        char dig10, dig11;
        int sm, i, r, num, peso;

        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i=0; i<9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posicao de '0' na tabela ASCII)
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else dig10 = (char)(r + 48); // converte no respectivo caractere numerico

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for(i=0; i<10; i++) {
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig11 = '0';
            else dig11 = (char)(r + 48);

            // Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
                this.cpf = CPF;
            else throw new InvalidParameterException();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDt_nascimento() {
        return dt_Nascimento;
    }

    public void setDt_nascimento(LocalDate dt_nascimento) {
        this.dt_Nascimento = dt_nascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getTelefone() {
        return telefone;
    }

    public void setTelefone(long telefone) {
        this.telefone = telefone;
    }
}
