package tk.tfsthiagobr98.abeu.farmacia;

import java.sql.*;

public class Remedio {
    private long EAN;
    private String nome;
    private float valor;
    private int estoque;

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/farmacia", "root", "tfs1112uyt"
        );
    }

    public void salvar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "insert into remedio "
                    + "(ean, nome, valor, estoque)"
                    + "values (?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setLong(1, getEAN());
            ps.setString(2, getNome());
            ps.setFloat(3, getValor());
            ps.setInt(4, getEstoque());

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante o salvamento de remedio");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void alterar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "update remedio "
                    + "set nome=?, valor=?, estoque=? "
                    + "where (ean = ?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, getNome());
            ps.setFloat(2, getValor());
            ps.setInt(3, getEstoque());
            ps.setLong(4, getEAN());

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a alteração de remedio");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void excluir() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "delete from remedio where (ean = ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setLong(1, getEAN());
            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a exclusão de remedio");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public long getEAN() {
        return EAN;
    }

    public void setEAN(long EAN) {
        this.EAN = EAN;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }
}
