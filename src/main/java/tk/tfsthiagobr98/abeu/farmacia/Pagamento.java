package tk.tfsthiagobr98.abeu.farmacia;

import java.sql.*;

public class Pagamento {
    private long id_Pag;
    private float valor_Total;
    private float valor_Pago;
    private String form_Pagamento;

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/farmacia", "root", "tfs1112uyt"
        );
    }

    public void salvar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "insert into pagamento "
                    + "(id_Pag, valor_Total, valor_Pago, form_Pagamento)"
                    + "values (?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setLong(1, id_Pag);
            ps.setFloat(2, valor_Total);
            ps.setFloat(3, valor_Pago);

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante o salvamento de Pagamento");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void alterar()  throws Exception {
        try {
            Connection con = getConnection();
            String sql = "update pagamento "
                    + "set valor_Total=?, valor_Pago=?, form_Pagamento=?"
                    + "where (id_Pag = ?)";
            PreparedStatement ps = con.prepareStatement(sql);


            ps.setFloat(1, valor_Total);
            ps.setFloat(2, valor_Pago);
            ps.setLong(3, id_Pag);

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante o alteração de Pagamento");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void excluir() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "delete from pagamento where (id_Pag = ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setLong(1, id_Pag);
            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a exclusão de Pagamento");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    // Encapsulamentos

    public long getId_Pag() {
        return id_Pag;
    }

    public void setId_Pag(long id_Pag) {
        this.id_Pag = id_Pag;
    }

    public float getValor_Total() {
        return valor_Total;
    }

    public void setValor_Total(float valor_Total) {
        this.valor_Total = valor_Total;
    }

    public float getValor_Pago() {
        return valor_Pago;
    }

    public void setValor_Pago(float valor_Pago) {
        this.valor_Pago = valor_Pago;
    }

    public String getForm_Pagamento() {
        return form_Pagamento;
    }

    public void setForm_Pagamento(String form_Pagamento) {
        this.form_Pagamento = form_Pagamento;
    }
}
