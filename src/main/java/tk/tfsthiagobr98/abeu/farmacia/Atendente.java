package tk.tfsthiagobr98.abeu.farmacia;

import java.sql.*;
import java.time.LocalDate;

public class Atendente {
    private long cod_Atendente;
    private String nome;
    private LocalDate dt_Nascimento;

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/farmacia", "root", "tfs1112uyt"
        );
    }

    public void salvar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "insert into atendente "
                    + "(cod_Atendente, nome, dt_Nascimento)"
                    + "values (?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setLong(1, cod_Atendente);
            ps.setString(2, nome);
            ps.setDate(3, Date.valueOf(dt_Nascimento));

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante o salvamento de Atendente");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void alterar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "update atendente "
                    + "set nome=?, dt_Nascimento=? "
                    + "where (cod_Atendente = ?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, nome);
            ps.setDate(2, Date.valueOf(dt_Nascimento));
            ps.setLong(3, getCod_Atendente());

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a alteração de Atendente");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void excluir() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "delete from atendente where (cod_Atendente = ?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setLong(1, cod_Atendente);
            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a exclusão de Atendente");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public long getCod_Atendente() {
        return cod_Atendente;
    }

    public void setCod_Atendente(long cod_Atendente) {
        this.cod_Atendente = cod_Atendente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDt_Nascimento() {
        return dt_Nascimento;
    }

    public void setDt_Nascimento(LocalDate dt_Nascimento) {
        this.dt_Nascimento = dt_Nascimento;
    }
}
