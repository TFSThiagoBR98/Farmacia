package tk.tfsthiagobr98.abeu.farmacia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ItemPedido {
    private Long cod_Item;
    private Pedido pedido;
    private Remedio remedio;
    private Receita receita;
    private int quantidade;

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/farmacia", "root", "tfs1112uyt"
        );
    }

    public void salvar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "insert into itempedido "
                    + "(cod_Item, cod_Pedido, cod_Receita, ean_Remedio, quantidade)"
                    + "values (?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setLong(1, cod_Item);
            ps.setLong(2, pedido.getNr_Pedido());
            ps.setLong(3, receita.getCod_Receita());
            ps.setLong(4, remedio.getEAN());
            ps.setInt(5, quantidade);


            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante o salvamento de Pagamento");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void alterar()  throws Exception {
        try {
            Connection con = getConnection();
            String sql = "update pagamento "
                    + "set cod_Pedido=?, cod_Receita=?, ean_Remedio=?, quantidade=?"
                    + "where (cod_Item = ?)";
            PreparedStatement ps = con.prepareStatement(sql);


            ps.setLong(1, pedido.getNr_Pedido());
            ps.setLong(2, receita.getCod_Receita());
            ps.setLong(3, remedio.getEAN());
            ps.setInt(4, quantidade);
            ps.setLong(5, cod_Item);

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante o alteração de Pagamento");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void excluir() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "delete from itempedido where (cod_Item = ?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setLong(1, cod_Item);

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a exclusão de Pagamento");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public Long getCod_Item() {
        return cod_Item;
    }

    public void setCod_Item(Long cod_Item) {
        this.cod_Item = cod_Item;
    }

    public Remedio getRemedio() {
        return remedio;
    }

    public void setRemedio(Remedio remedio) {
        this.remedio = remedio;
    }

    public Receita getReceita() {
        return receita;
    }

    public void setReceita(Receita receita) {
        this.receita = receita;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
