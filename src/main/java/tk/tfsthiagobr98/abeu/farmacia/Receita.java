package tk.tfsthiagobr98.abeu.farmacia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Receita {
    private long cod_Receita;
    private String crm_Medico;

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/farmacia", "root", "tfs1112uyt"
        );
    }

    public void salvar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "insert into receita "
                    + "(cod_Receita, crm_Medico)"
                    + "values (?,?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setLong(1,cod_Receita);
            ps.setString(2, crm_Medico);

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante o salvamento de receita");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void alterar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "update receita "
                    + "set crm_Medico=?"
                    + "where (cod_Receita = ?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, crm_Medico);
            ps.setLong(2, cod_Receita);

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a alteração de receita");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void excluir() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "delete from receita where (cod_Receita = ?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setLong(1,cod_Receita);

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a exclusão de receita");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public long getCod_Receita() {
        return cod_Receita;
    }

    public void setCod_Receita(long cod_Receita) {
        this.cod_Receita = cod_Receita;
    }

    public String getCrm_Medico() {
        return crm_Medico;
    }

    public void setCrm_Medico(String crm_Medico) {
        this.crm_Medico = crm_Medico;
    }
}
