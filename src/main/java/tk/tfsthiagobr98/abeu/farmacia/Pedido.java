package tk.tfsthiagobr98.abeu.farmacia;

import java.sql.*;
import java.util.ArrayList;

public class Pedido {
    private long nr_Pedido;
    private Cliente cliente;
    private Atendente atendente;
    private Pagamento pagamento;
    private ArrayList<ItemPedido> itens;

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/farmacia", "root", "tfs1112uyt"
        );
    }

    public void salvar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "insert into pedido "
                    + "(nr_Pedido, cpf_Cliente, cod_Atendente, cod_Pagamento)"
                    + "values (?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setLong(1, nr_Pedido);
            ps.setString(2, cliente.getCPF());
            ps.setLong(3,atendente.getCod_Atendente());
            ps.setLong(4,pagamento.getId_Pag());

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante o salvamento de Pedido");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void alterar() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "update pedido "
                    + "set cpf_Cliente=?, cod_Atendente=?, cod_Pagamento=?"
                    + "where (nr_Pedido = ?)";
            PreparedStatement ps = con.prepareStatement(sql);


            ps.setString(1, cliente.getCPF());
            ps.setLong(2,atendente.getCod_Atendente());
            ps.setLong(3,pagamento.getId_Pag());
            ps.setLong(4, nr_Pedido);

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a alteração de Pedido");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public void excluir() throws Exception {
        try {
            Connection con = getConnection();
            String sql = "delete from pedido where (nr_Pedido = ?)";
            PreparedStatement ps = con.prepareStatement(sql);

            ps.setLong(1, nr_Pedido);

            if (ps.executeUpdate() <= 0) {
                throw new SQLException("Erro durante a exclusão de Pedido");
            }
            ps.close();
            con.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public long getNr_Pedido() {
        return nr_Pedido;
    }

    public void setNr_Pedido(long nr_Pedido) {
        this.nr_Pedido = nr_Pedido;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Atendente getAtendente() {
        return atendente;
    }

    public void setAtendente(Atendente atendente) {
        this.atendente = atendente;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    public ArrayList<ItemPedido> getItens() {
        return itens;
    }

    public void setItens(ArrayList<ItemPedido> itens) {
        this.itens = itens;
    }
}
