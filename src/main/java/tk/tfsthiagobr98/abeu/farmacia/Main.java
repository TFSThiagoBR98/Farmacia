package tk.tfsthiagobr98.abeu.farmacia;

import java.lang.String;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        System.out.println("TFSThiagoBR98 Prótotipo Farmácia v1");
        System.out.println("Copyright (C) TFSThiagoBR98. Restrito somente para uso acadêmico");
        try {
            Cliente d = new Cliente();
            d.setCPF("47886197097");
            d.setNome("Exemplo da Silva");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            d.setDt_nascimento(LocalDate.parse("11/05/1995", formatter));
            d.setEmail("exemplo@gmail.com");
            d.setTelefone(27539994);
            d.salvar();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
